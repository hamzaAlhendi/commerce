"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const auth_module_1 = require("./auth/auth.module");
const company_module_1 = require("./company/company.module");
const company_entiti_1 = require("./company/entities/company.entiti");
const customer_module_1 = require("./customer/customer.module");
const customer_entnty_1 = require("./customer/entities/customer.entnty");
const order_details_entity_1 = require("./order-details/entities/order-details.entity");
const order_details_module_1 = require("./order-details/order-details.module");
const order_entity_1 = require("./order/entities/order.entity");
const order_module_1 = require("./order/order.module");
const product_entity_1 = require("./product/entities/product.entity");
const product_module_1 = require("./product/product.module");
const user_entity_1 = require("./user/entity/user.entity");
const user_module_1 = require("./user/user.module");
const logg_controller_1 = require("./logg/logg.controller");
const logg_module_1 = require("./logg/logg.module");
let AppModule = class AppModule {
    constructor(dataSource) {
        this.dataSource = dataSource;
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'root',
                password: '123456789',
                database: 'new_schema',
                entities: [user_entity_1.User, customer_entnty_1.Customer, company_entiti_1.Company, product_entity_1.Product, order_entity_1.Order, order_details_entity_1.OrderDetails],
                synchronize: true,
                autoLoadEntities: true,
            }),
            auth_module_1.AuthModule,
            user_module_1.UserModule,
            customer_module_1.CustomerModule,
            company_module_1.CompanyModule,
            product_module_1.ProductModule,
            user_module_1.UserModule,
            order_module_1.OrderModule,
            order_details_module_1.OrderDetailsModule,
            logg_module_1.LoggModule
        ],
        controllers: [app_controller_1.AppController, logg_controller_1.LoggController],
        providers: [app_service_1.AppService],
    }),
    __metadata("design:paramtypes", [typeorm_2.DataSource])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map