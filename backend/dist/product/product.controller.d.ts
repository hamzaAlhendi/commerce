import { ProductService } from './product.service';
export declare class ProductController {
    private productService;
    constructor(productService: ProductService);
    getproducts(): Promise<import("./entities/product.entity").Product[]>;
}
