import { Repository } from 'typeorm';
import { Product } from './entities/product.entity';
export declare class ProductService {
    private productRepository;
    constructor(productRepository: Repository<Product>);
    findAll(): Promise<Product[]>;
    findProductById(productId: number): Promise<Product>;
}
