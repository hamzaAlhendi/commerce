import { OrderDetails } from "src/order-details/entities/order-details.entity";
export declare class Product {
    productId: number;
    name: string;
    photoUrl: string;
    price: number;
    odetails: OrderDetails[];
}
