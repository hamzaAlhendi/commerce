export declare class OrderDto {
    userId: number;
    price: number;
    products: {
        counter: number;
        productId: number;
    }[];
}
