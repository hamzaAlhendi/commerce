import { OrderDetails } from "src/order-details/entities/order-details.entity";
import { User } from "src/user/entity/user.entity";
export declare class Order {
    orderId: number;
    createdAt?: Date;
    user: User;
    totalPrice: number;
    orderDetails: OrderDetails[];
}
