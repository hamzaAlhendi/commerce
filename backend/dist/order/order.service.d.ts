import { OrderDetails } from 'src/order-details/entities/order-details.entity';
import { OrderDetailsService } from 'src/order-details/order-details.service';
import { UserService } from 'src/user/user.service';
import { DataSource, Repository } from 'typeorm';
import { OrderDto } from './dto/order.dto';
import { Order } from './entities/order.entity';
export declare class OrderService {
    private orderRepository;
    private orderDetailsRepository;
    private userService;
    private orderDetailsService;
    private dataSource;
    constructor(orderRepository: Repository<Order>, orderDetailsRepository: Repository<OrderDetails>, userService: UserService, orderDetailsService: OrderDetailsService, dataSource: DataSource);
    saveOrder(order: OrderDto): Promise<any>;
    getAll(): Promise<Order[]>;
    getUserOrders(userId: any): Promise<Order[]>;
}
