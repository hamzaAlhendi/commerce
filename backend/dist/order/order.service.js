"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const order_details_entity_1 = require("../order-details/entities/order-details.entity");
const order_details_service_1 = require("../order-details/order-details.service");
const user_service_1 = require("../user/user.service");
const typeorm_2 = require("typeorm");
const order_entity_1 = require("./entities/order.entity");
let OrderService = class OrderService {
    constructor(orderRepository, orderDetailsRepository, userService, orderDetailsService, dataSource) {
        this.orderRepository = orderRepository;
        this.orderDetailsRepository = orderDetailsRepository;
        this.userService = userService;
        this.orderDetailsService = orderDetailsService;
        this.dataSource = dataSource;
    }
    async saveOrder(order) {
        const user = await this.userService.findOneById(order.userId);
        const neworder = this.orderRepository.create();
        neworder.user = user;
        neworder.totalPrice = order.price;
        let createdOrder;
        let createdOrderDetails;
        return await this.dataSource.transaction(async (transactionalEntityManager) => {
            createdOrder = await this.orderRepository.save(neworder);
            createdOrderDetails = await this.orderDetailsService.saveOrderDetails(order, createdOrder.orderId);
        }).then((value) => {
            return { createdOrder, createdOrderDetails };
        }, () => console.log("error happened"));
    }
    getAll() {
        return this.orderRepository.find();
    }
    async getUserOrders(userId) {
        let orders = await this.orderRepository.find({ relations: ['user'] });
        orders = orders.filter(order => order.user.id == userId);
        let details = await this.orderDetailsRepository.find({ relations: ['order', 'product'] });
        orders.forEach(async (order) => {
            details = (await details).filter(item => {
                return item.order.orderId === order.orderId;
            });
            order.orderDetails = details;
        });
        return orders;
    }
};
OrderService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(order_entity_1.Order)),
    __param(1, (0, typeorm_1.InjectRepository)(order_details_entity_1.OrderDetails)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        user_service_1.UserService,
        order_details_service_1.OrderDetailsService,
        typeorm_2.DataSource])
], OrderService);
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map