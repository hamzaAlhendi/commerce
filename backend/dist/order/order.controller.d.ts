import { OrderDto } from './dto/order.dto';
import { Order } from './entities/order.entity';
import { OrderService } from './order.service';
export declare class OrderController {
    private orderservice;
    constructor(orderservice: OrderService);
    getAll(): void;
    saveOrder(order: OrderDto): Promise<any>;
    getUserOrders(uid: any): Promise<Order[]>;
}
