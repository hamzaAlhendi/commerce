export declare class Customer {
    id: string;
    name: string;
    phoneNumber: string;
    email: string;
    photo: string;
}
