import { Product } from "src/product/entities/product.entity";
import { User } from "src/user/entity/user.entity";
export declare class Bill {
    billId: number;
    user: User;
    products: Product[];
}
