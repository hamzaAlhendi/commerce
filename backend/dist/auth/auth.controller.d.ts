import { AuthService } from './auth.service';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(req: any): Promise<{
        user: any;
        access_token: string;
    }>;
    register(req: any): Promise<{
        user: {
            id: number;
            email: string;
            firstName: string;
            lastName: string;
            created_at?: Date;
            updated_at?: Date;
            phoneNumber: string;
            photoUrl: string;
            city: string;
            orders: import("../order/entities/order.entity").Order[];
            loggs: import("../logg/entity/logg.entity").Logg[];
        };
        access_token: string;
    }>;
    getProfile(req: any): any;
}
