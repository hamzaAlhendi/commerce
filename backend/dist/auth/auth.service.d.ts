import { UserService } from 'src/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { loginUserDto } from './dto/login.dto';
import { userRegisterDto } from 'src/user/dto/userregister.dto';
export declare class AuthService {
    private jwtService;
    private userService;
    currentUser: any;
    constructor(jwtService: JwtService, userService: UserService);
    validateUser(uemail: string, upassword: string): Promise<any>;
    login(user: loginUserDto): Promise<{
        user: any;
        access_token: string;
    }>;
    register(registeruser: userRegisterDto): Promise<{
        user: {
            id: number;
            email: string;
            firstName: string;
            lastName: string;
            created_at?: Date;
            updated_at?: Date;
            phoneNumber: string;
            photoUrl: string;
            city: string;
            orders: import("../order/entities/order.entity").Order[];
            loggs: import("../logg/entity/logg.entity").Logg[];
        };
        access_token: string;
    }>;
    decodeToken(token: any): any;
}
