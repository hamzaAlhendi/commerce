export declare class createUserDto {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
}
