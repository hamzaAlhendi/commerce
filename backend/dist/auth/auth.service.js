"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../user/user.service");
const bcrypt = require("bcrypt");
const jwt_1 = require("@nestjs/jwt");
let AuthService = class AuthService {
    constructor(jwtService, userService) {
        this.jwtService = jwtService;
        this.userService = userService;
        this.currentUser = null;
    }
    async validateUser(uemail, upassword) {
        this.currentUser = await this.userService.findOne(uemail);
        if (this.currentUser &&
            bcrypt.compareSync(upassword, this.currentUser.password)) {
            console.log(this.currentUser.password, await bcrypt.hash(upassword, 10));
            const _a = await this.currentUser, { password } = _a, result = __rest(_a, ["password"]);
            return result;
        }
        return null;
    }
    async login(user) {
        const payload = {
            user: {
                email: user.email,
                password: user.password,
            },
        };
        console.log(this.currentUser);
        return {
            user: this.currentUser,
            access_token: this.jwtService.sign(payload),
        };
    }
    async register(registeruser) {
        if (await this.userService.findOne(registeruser.email))
            throw new common_1.ConflictException('email' + registeruser.email + 'already exists');
        else {
            const response = await this.userService.create(registeruser);
            console.log(response);
            const { password } = response, result = __rest(response, ["password"]);
            const payload = {
                user: {
                    email: registeruser.email,
                    password: registeruser.password,
                },
            };
            return {
                user: result,
                access_token: this.jwtService.sign(payload),
            };
        }
    }
    decodeToken(token) {
        return this.jwtService.decode(token);
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        user_service_1.UserService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map