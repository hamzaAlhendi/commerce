import { Order } from "src/order/entities/order.entity";
import { Product } from "src/product/entities/product.entity";
export declare class OrderDetails {
    orderdetailsId: number;
    order: Order;
    product: Product;
    numberOfItems: number;
}
