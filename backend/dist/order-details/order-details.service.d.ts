import { OrderDto } from 'src/order/dto/order.dto';
import { ProductService } from 'src/product/product.service';
import { Repository } from 'typeorm';
import { OrderDetails } from './entities/order-details.entity';
export declare class OrderDetailsService {
    private orderdetailsRepository;
    private productsService;
    constructor(orderdetailsRepository: Repository<OrderDetails>, productsService: ProductService);
    saveOrderDetails(order: OrderDto, orderId: any): any[];
}
