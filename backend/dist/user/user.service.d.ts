import { Repository } from 'typeorm';
import { userRegisterDto } from './dto/userregister.dto';
import { User } from './entity/user.entity';
export declare class UserService {
    private usersRepository;
    constructor(usersRepository: Repository<User>);
    findAll(): Promise<User[]>;
    findOne(email: string): Promise<any>;
    findOneById(id: number): Promise<any>;
    remove(id: string): Promise<void>;
    create(createUser: userRegisterDto): Promise<User>;
}
