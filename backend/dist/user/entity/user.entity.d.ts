import { Logg } from 'src/logg/entity/logg.entity';
import { Order } from 'src/order/entities/order.entity';
export declare class User {
    id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    created_at?: Date;
    updated_at?: Date;
    phoneNumber: string;
    photoUrl: string;
    city: string;
    orders: Order[];
    loggs: Logg[];
}
