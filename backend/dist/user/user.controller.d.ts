import { userLogindto } from './dto/userlogin.dto';
import { userRegisterDto } from './dto/userregister.dto';
import { User } from './entity/user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    findall(): Promise<User[]>;
    registerUser(userDto: userRegisterDto): Promise<User>;
    login(userDto: userLogindto): Promise<User>;
}
