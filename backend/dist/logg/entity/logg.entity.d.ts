import { User } from "src/user/entity/user.entity";
export declare class Logg {
    id: number;
    logMessage: string;
    time: string;
    user: User;
}
