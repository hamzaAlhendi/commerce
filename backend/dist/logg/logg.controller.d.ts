import { LoggService } from './logg.service';
export declare class LoggController {
    private logService;
    constructor(logService: LoggService);
    saveLog(data: any): void;
}
