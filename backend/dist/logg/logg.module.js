"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_module_1 = require("../user/user.module");
const logg_entity_1 = require("./entity/logg.entity");
const logg_service_1 = require("./logg.service");
let LoggModule = class LoggModule {
};
LoggModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([logg_entity_1.Logg]), user_module_1.UserModule],
        controllers: [],
        providers: [logg_service_1.LoggService],
        exports: [logg_service_1.LoggService]
    })
], LoggModule);
exports.LoggModule = LoggModule;
//# sourceMappingURL=logg.module.js.map