import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { Logg } from './entity/logg.entity';
export declare class LoggService {
    private logrepository;
    private userService;
    constructor(logrepository: Repository<Logg>, userService: UserService);
    saveLog(data: any): Promise<void>;
}
