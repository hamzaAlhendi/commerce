import { UserService } from './user/user.service';
export declare class AppService {
    private userService;
    constructor(userService: UserService);
    getHello(): string;
}
