import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderDto } from 'src/order/dto/order.dto';
import { ProductService } from 'src/product/product.service';
import { Repository } from 'typeorm';
import { OrderDetails } from './entities/order-details.entity';

@Injectable()
export class OrderDetailsService {

    constructor(
        @InjectRepository(OrderDetails)
        private orderdetailsRepository:Repository<OrderDetails>,
        private productsService : ProductService

    ){}

    saveOrderDetails(order:OrderDto,orderId){
        const cretedOrderDetails = [];
        
        
        order.products.forEach( async product=>{
            console.log("product", product );
            
            const details = this.orderdetailsRepository.create();
            const productDetails = this.productsService.findProductById(product.productId)
            
            
            details.numberOfItems = product.counter;
            details.product = await this.productsService.findProductById(product.productId)
            details.order = orderId;
            
            const newDetails = this.orderdetailsRepository.save(details)
            
            cretedOrderDetails.push(newDetails)
            
        })
        return cretedOrderDetails;
        
    }
}
