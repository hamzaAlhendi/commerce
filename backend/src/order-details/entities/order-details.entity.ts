import { timestamp } from "rxjs";
import { Order } from "src/order/entities/order.entity";
import { Product } from "src/product/entities/product.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class OrderDetails{

    @PrimaryGeneratedColumn()
    orderdetailsId:number;

    

    @ManyToOne(()=>Order,(order)=>order.orderId)
    @JoinColumn({name:'order_id'})
    order:Order;


    @ManyToOne(()=>Product,(product)=>product.productId)
    @JoinColumn({name:'product_id'})
    product:Product;
    
    
    @Column()
    numberOfItems:number;

    
}