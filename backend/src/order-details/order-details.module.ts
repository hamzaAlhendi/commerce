import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from 'src/product/product.module';
import { OrderDetails } from './entities/order-details.entity';
import { OrderDetailsController } from './order-details.controller';
import { OrderDetailsService } from './order-details.service';

@Module({
  imports: [TypeOrmModule.forFeature([OrderDetails]),ProductModule],

  controllers: [OrderDetailsController],
  providers: [OrderDetailsService],
  exports:[OrderDetailsService,TypeOrmModule.forFeature([OrderDetails])]
})
export class OrderDetailsModule {}
