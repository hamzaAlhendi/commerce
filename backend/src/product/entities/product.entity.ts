
import { OrderDetails } from "src/order-details/entities/order-details.entity";
import { Column, Entity,  ManyToOne,  OneToMany,  PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product{
  @PrimaryGeneratedColumn()
  productId: number;

  @Column()
  name:string;

  @Column()
  photoUrl:string;

  @Column()
  price:number;


  @OneToMany(() => OrderDetails,(odetails) => odetails.orderdetailsId)
  odetails:OrderDetails[];
//   @Column({default:null})
//   categori:string;

//   @Column({default:null})
//   brand:string;


//   @Column({default:null})
//   company:string;
}