import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Product } from './entities/product.entity';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private productRepository: Repository<Product>
    ){}

    findAll(){
        return this.productRepository.find()
    }


    async findProductById(productId:number) : Promise<Product>{
        console.log("productid:",productId)
        return await this.productRepository.findOne({where : {productId: productId}});
        
        

    }

    
}
