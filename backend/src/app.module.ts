import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CompanyModule } from './company/company.module';
import { Company } from './company/entities/company.entiti';
import { CustomerModule } from './customer/customer.module';
import { Customer } from './customer/entities/customer.entnty';
import { OrderDetails } from './order-details/entities/order-details.entity';
import { OrderDetailsModule } from './order-details/order-details.module';
import { Order } from './order/entities/order.entity';
import { OrderModule } from './order/order.module';
import { Product } from './product/entities/product.entity';
import { ProductModule } from './product/product.module';
import { User } from './user/entity/user.entity';
import { UserModule } from './user/user.module';
import { LoggController } from './logg/logg.controller';
import { LoggModule } from './logg/logg.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '123456789',
      database: 'new_schema',
      entities: [User, Customer, Company,Product,Order,OrderDetails],
      synchronize: true,
      autoLoadEntities: true,
    }),
    AuthModule,
    UserModule,
    CustomerModule,
    CompanyModule,
    ProductModule,
    UserModule,
    OrderModule,
    OrderDetailsModule,
    LoggModule
  ],
  controllers: [AppController, LoggController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
