import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from 'src/user/user.module';
import { Logg} from './entity/logg.entity'
import { LoggService } from './logg.service';

@Module({
    imports:[TypeOrmModule.forFeature([Logg]),UserModule],
    controllers:[],
    providers: [LoggService],
    exports:[LoggService]
})
export class LoggModule {}
