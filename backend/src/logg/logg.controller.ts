import { Body, Controller, Post } from '@nestjs/common';
import { LoggService } from './logg.service';

@Controller('logg')
export class LoggController {

    constructor(private logService:LoggService){

    }

    @Post('save')
    saveLog(@Body() data:any){
        console.log(data);
        
        this.logService.saveLog(data)
    }
}
