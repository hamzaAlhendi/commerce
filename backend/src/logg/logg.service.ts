import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { Logg } from './entity/logg.entity';

@Injectable()
export class LoggService {

    constructor(
        @InjectRepository(Logg)
        private logrepository:Repository<Logg>,
        private userService:UserService
    )
    {

    }
    async saveLog(data){
        const logg = new Logg()
        logg.logMessage = data.logMessage;
        logg.user = await this.userService.findOne(data.userEmail)
        this.logrepository.save(logg)
    }
}
