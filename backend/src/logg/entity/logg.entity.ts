import { User } from "src/user/entity/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Logg{

    @PrimaryGeneratedColumn()
    id:number;


    @Column({length:300})
    logMessage:string;

    @Column({type: 'timestamp',default: ()=> 'CURRENT_TIMESTAMP'})
    time:string


    @ManyToOne(()=> User,(user)=> user.orders)
    @JoinColumn()
    user:User;

    
}