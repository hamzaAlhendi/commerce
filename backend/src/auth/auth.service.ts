
import { ConflictException, Injectable } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { loginUserDto } from './dto/login.dto';
import { createUserDto } from './dto/register.dto';
import { userRegisterDto } from 'src/user/dto/userregister.dto';

@Injectable()
export class AuthService {
  
  currentUser = null;

  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  async validateUser(uemail: string, upassword: string) {
    this.currentUser = await this.userService.findOne(uemail);
    if (
      this.currentUser &&
      bcrypt.compareSync(upassword,this.currentUser.password)
      
    ) {
      console.log(this.currentUser.password,await bcrypt.hash(upassword,10))
      const { password, ...result } = await this.currentUser;
      return result;
    }
    return null;
  }

  async login(user: loginUserDto) {
    // console.log(user.user);
    const payload = {
      user: {
        email: user.email,
        password: user.password,
      },
    };
    // console.log({payload});
    console.log(this.currentUser);
    
    return {
      user:this.currentUser,
      access_token: this.jwtService.sign(payload),
    };
  }

  async register(registeruser:userRegisterDto) {
    if(await this.userService.findOne(registeruser.email))
      throw new ConflictException('email' + registeruser.email + 'already exists')
    else{
      const response = await this.userService.create(registeruser);
      
    console.log(response);

      const { password, ...result } = response;

      const payload = {
        user: {
          email: registeruser.email,
          password: registeruser.password,
        },
      };

      
      return {
        user:result,
        access_token: this.jwtService.sign(payload),
      };
    }
  
  }

  decodeToken(token): any {
    return this.jwtService.decode(token);
  }
}
