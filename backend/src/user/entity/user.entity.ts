import { Logg} from 'src/logg/entity/logg.entity';
import { Order } from 'src/order/entities/order.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm';

@Entity()
export class User {
  
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;

  @Column({ type: 'datetime', nullable: true })
  updated_at?: Date;

  @Column()
  phoneNumber:string;

  @Column({default:"https://cdn.pixabay.com/photo/2012/04/01/18/22/user-23874_960_720.png"})
  photoUrl:string;

  @Column()
  city:string;

  @OneToMany(()=> Order,(order)=> order.user)
  @JoinColumn()
  orders:Order[];


  @OneToMany(()=> Logg,(logg)=> logg.id)
  @JoinColumn()
  loggs:Logg[];
}
