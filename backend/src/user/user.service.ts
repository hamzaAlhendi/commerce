import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { userLogindto } from './dto/userlogin.dto';
import { userRegisterDto } from './dto/userregister.dto';
import { User } from './entity/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}
  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async findOne(email: string): Promise<any> {
    return await this.usersRepository.findOne({ where: { email } });
  }

  async findOneById(id:number):Promise<any>{
    return this.usersRepository.findOne({where:{id}})
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  async create(createUser: userRegisterDto): Promise<User> {
    const saltOrRounds = 10;
    const password = createUser.password;
    const hash = await bcrypt.hashSync(password, saltOrRounds);
    createUser.password = hash;
    return this.usersRepository.save(createUser);
  }
}
