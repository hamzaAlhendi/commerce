import { Body, Controller, Get, Post } from '@nestjs/common';
import { userLogindto } from './dto/userlogin.dto';
import { userRegisterDto } from './dto/userregister.dto';
import { User } from './entity/user.entity';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}
  // @Get(':id')
  // findUser(): string {
  //   return 'user found';
  // }
  // @Get('all')
  findall() {
    return this.userService.findAll();
  }

  // @Post('register')
  registerUser(@Body() userDto: userRegisterDto): Promise<User> {
    return this.userService.create(userDto);
  }

  // @Post('login')
  login(@Body() userDto: userLogindto): Promise<User> {
    return null;
  }
}
