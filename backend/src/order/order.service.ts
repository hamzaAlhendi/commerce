import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderDetails } from 'src/order-details/entities/order-details.entity';
import { OrderDetailsService } from 'src/order-details/order-details.service';
import { UserService } from 'src/user/user.service';
import { DataSource, Repository } from 'typeorm';
import { OrderDto } from './dto/order.dto';
import { Order } from './entities/order.entity';

@Injectable()
export class OrderService {

    constructor(
        @InjectRepository(Order)
        private orderRepository:Repository<Order>,
        @InjectRepository(OrderDetails)
        private orderDetailsRepository:Repository<OrderDetails>,
        private userService:UserService,
        private orderDetailsService:OrderDetailsService,
        private dataSource : DataSource
    ){}


    async saveOrder(order:OrderDto) : Promise<any>{
        const user = await this.userService.findOneById(order.userId)
        const neworder = this.orderRepository.create()
        neworder.user = user;
        neworder.totalPrice = order.price;
        let createdOrder;
        let createdOrderDetails;
        return await this.dataSource.transaction(async (transactionalEntityManager) => {
            createdOrder = await this.orderRepository.save(neworder);
            createdOrderDetails = await this.orderDetailsService.saveOrderDetails(order,createdOrder.orderId)

        }).then((value) => {
            return {createdOrder,createdOrderDetails}
        },
            ()=>console.log("error happened")
        )
    }

    getAll(){
        return  this.orderRepository.find()
    }

    async getUserOrders(userId){
        
        let  orders = await this.orderRepository.find({relations : ['user']})

        orders = orders.filter(order => order.user.id == userId)
        let details = await this.orderDetailsRepository.find({relations:['order','product']})
        orders.forEach(async order => {
            details = (await details).filter(item => {

                return item.order.orderId === order.orderId
            })
            
            order.orderDetails = details


        })
        return orders
    }

}
