import { type } from "os";
import { OrderDetails } from "src/order-details/entities/order-details.entity";
import { Product } from "src/product/entities/product.entity";
import { User } from "src/user/entity/user.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Order{

    @PrimaryGeneratedColumn()
    orderId:number;

    @Column({type: 'timestamp',default: ()=> 'CURRENT_TIMESTAMP'})
    createdAt?:Date;

    
    @ManyToOne(()=> User,(user)=> user.orders)
    @JoinColumn()
    user:User;


    @Column()
    totalPrice:number;

    
    @OneToMany(()=> OrderDetails, (orderdetails)=>orderdetails.orderdetailsId)
    orderDetails:OrderDetails[];
}