import { Body, Controller, Get, Param, Post, Request } from '@nestjs/common';
import { OrderDto } from './dto/order.dto';
import { Order } from './entities/order.entity';
import { OrderService } from './order.service';

@Controller('order')
export class OrderController {

    constructor(private orderservice:OrderService){

    }


    @Get('all')
    getAll(){
         console.log(this.orderservice.getAll())
    }

    @Post('save')
    saveOrder(@Body()order:OrderDto){
        return this.orderservice.saveOrder(order)   
    }

    @Get('userorders/:userid',)
    getUserOrders(@Param('userid') uid ){
        console.log(uid)
        return this.orderservice.getUserOrders(uid)
    }


}
