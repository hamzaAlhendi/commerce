import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../authentication/auth.service';
import { Product } from '../models/product.model';
import { Profile } from '../models/profile.model';
import { User } from '../models/user.model';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() user : Profile|null = null;

  products :{counter:number,product:Product}[] = []

  number = 2;

  constructor(private router:Router,private authService:AuthService,public productsService:ProductsService) { }

  ngOnInit(): void {
    this.fetchProducts()
  }

  logout(){
    this.authService.logout()
    this.router.navigate([""])
  }

  fetchProducts(){
    return this.productsService.getCurrentUserProducts()
  }

  totalPrice(){
    return this.productsService.getProductsTotalPrice()

  }

  removeFromCard(product:Product){
    this.productsService.removeProductFromTheList(product)
  }
}
