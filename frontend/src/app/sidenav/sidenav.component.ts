import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  withitemsCategories: {key: string, values: string[]}[] = [{key:"SPORTS",values:["NIKE",
    "UNDER ARMOUR",
    "ADIDAS",
    "PUMA",
    "ASICS"]},
    {key:"MEN",values:["FENDI",
    "GUESS",
    "VALENTINO",
    "DIOR",
    "VERSACE"]},
    {key:"WOMEN",values:["FENDI",
    "GUESS",
    "VALENTINO",
    "DIOR",
    "VERSACE"]}
  ]

  noitemsCategories = ["KIDS",
    "FASHION",
    "HOUSEHOLDS",
    "INTERIORS",
    "CLOTHING",
    "BAGS",
    "SHOES"]

  brands = ["ACNE",
    "GRÜNE ERDE",
    "ALBIRO",
    "RONHILL",
    "ODDMOLLY",
    "BOUDESTIJN",
    "RÖSCH CREATIVE CULTURE"]
  constructor() { }

  ngOnInit(): void {
  }

}
