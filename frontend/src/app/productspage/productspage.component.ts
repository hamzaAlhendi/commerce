import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { AuthService } from '../authentication/auth.service';
import { Product } from '../models/product.model';
import { Profile } from '../models/profile.model';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-productspage',
  templateUrl: './productspage.component.html',
  styleUrls: ['./productspage.component.css']
})
export class ProductspageComponent implements OnInit {

  products:Product[] = [];
  currentUser:Profile|null = null;
  lowValue = 0;
  highValue = 12;

  withitemsCategories: {key: string, values: string[]}[] = [{key:"SPORTS",values:["NIKE",
  "UNDER ARMOUR",
  "ADIDAS",
  "PUMA",
  "ASICS"]},
  {key:"MEN",values:["FENDI",
  "GUESS",
  "VALENTINO",
  "DIOR",
  "VERSACE"]},
  {key:"WOMEN",values:["FENDI",
  "GUESS",
  "VALENTINO",
  "DIOR",
  "VERSACE"]}
]

noitemsCategories = ["KIDS",
  "FASHION",
  "HOUSEHOLDS",
  "INTERIORS",
  "CLOTHING",
  "BAGS",
  "SHOES"]

brands = ["ACNE",
  "GRÜNE ERDE",
  "ALBIRO",
  "RONHILL",
  "ODDMOLLY",
  "BOUDESTIJN",
  "RÖSCH CREATIVE CULTURE"]

  constructor(public authService:AuthService, private productService:ProductsService) { }

  ngOnInit(): void {
    this.productService.getproducts().subscribe((products:any) => {
      this.products = products

    })

    this.authService.getcurrentUser().subscribe(value => {
      this.currentUser = value
    })
  }

  nextPage(event: PageEvent){
    this.lowValue = event.pageIndex * event.pageSize > this.products.length ? this.lowValue :  event.pageIndex * event.pageSize;
    this.highValue = this.lowValue + event.pageSize > this.products.length ? this.products.length : this.lowValue+event.pageSize;
    console.log(this.lowValue,this.highValue)
    return event;
  }

}
