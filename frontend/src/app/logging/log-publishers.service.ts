import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';
import { LogPublisher } from './log-publisher';
import { LogToConsole } from './log-to-console';
import { LogToDb } from './log-to-db';

@Injectable({
  providedIn: 'root'
})
export class LogPublishersService {




  constructor(private http:HttpClient,private userService:UserService) {
    this.buildPublishers()
   }

  publishers: LogPublisher[] = [];


  buildPublishers(): void {
    this.publishers.push(new LogToDb(this.userService,this.http))
    this.publishers.push(new LogToConsole());
  }
}
