import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { UserService } from "../services/user.service";
import { LogEntry } from "./log-entry";
import { LogPublisher } from "./log-publisher";

export class LogToDb extends LogPublisher{


  url = 'http://localhost:3000/logg/save'

  constructor(private userService:UserService,private http:HttpClient){
    super()
  }

  log(message: LogEntry): Observable<boolean> {
    console.log('in db service')
    let userEmail = this.userService.getcurrentUserEmail()
    let logMessage = message.buildLogString();
    this.http.post(this.url,{userEmail,logMessage}).subscribe(value=>console.log(value))
    return of(true)

  }

  clear(): Observable<boolean> {
    return of(true)
  }
}
