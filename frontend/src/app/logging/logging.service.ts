import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { LogEntry } from './log-entry';
import { LogLevel } from './log-level';
import { LogPublisher } from './log-publisher';
import { LogPublishersService } from './log-publishers.service';
import { LogToDb } from './log-to-db';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  level:LogLevel = LogLevel.All;
  logWithDate:boolean = true;



  publishers : LogPublisher[] = []
  constructor(private logPublishersService:LogPublishersService) {
    this.publishers = this.logPublishersService.publishers;
  }

  debug(msg: string, ...optionalParams:any[]) {
    this.writeToLog(msg, LogLevel.Debug, optionalParams);
  }

  info(msg: string, ...optionalParams:any[]) {
      this.writeToLog(msg, LogLevel.Info, optionalParams);
  }

  warn(msg: string, ...optionalParams:any[]) {
      this.writeToLog(msg, LogLevel.Warn, optionalParams);
  }

  error(msg: string, ...optionalParams:any[]) {
      this.writeToLog(msg, LogLevel.Error, optionalParams);
  }

  fatal(msg: string, ...optionalParams:any[]) {
      this.writeToLog(msg, LogLevel.Fatal, optionalParams);
  }

  log(msg: string, ...optionalParams:any[]) {
      this.writeToLog(msg, LogLevel.All, optionalParams);
  }

  success(msg: string, ...optionalParams:any[]){
    this.writeToLog(msg, LogLevel.Success, optionalParams);
  }

  private writeToLog(msg: string, level: LogLevel, params: any[]) {
    if (this.shouldLog(level)) {
        let entry: LogEntry = new LogEntry();
        entry.message = msg;
        entry.level = level;
        entry.extraInfo = params;
        entry.logWithDate = this.logWithDate;
        if(params.find(parametar => parametar.username))
          for(let publisher of this.publishers){

            publisher.log(entry)
            console.log(entry)
          }
        else{
          this.publishers.forEach(publisher => {
            if(typeof(publisher) != typeof(LogToDb)){
              publisher.log(entry)
              console.log(typeof(publisher))
            }
          })
        }

    }
  }

  private shouldLog(level: LogLevel): boolean {
    let ret: boolean = false;
    if ((level >= this.level && level !== LogLevel.Off) || this.level === LogLevel.All) {
        ret = true;
    }
    return ret;
  }
}
