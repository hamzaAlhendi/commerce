import { Observable, of } from "rxjs";
import { LogEntry } from "./log-entry";
import { LogPublisher } from "./log-publisher";
import {} from 'rxjs/operators'

export class LogToConsole extends LogPublisher {

  log(message: LogEntry): Observable<boolean> {
    console.log(message.buildLogString())
    return of(true);
  }
  clear(): Observable<boolean> {
    console.clear();
    return of(true);

  }

}
