import { Observable } from "rxjs";
import { LogEntry } from "./log-entry";

export abstract class LogPublisher {
    location: string="";
    abstract log(message: LogEntry):
    Observable<boolean>
    abstract clear(): Observable<boolean>;
}
