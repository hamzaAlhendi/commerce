import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, Subject } from 'rxjs';
import { Profile } from '../models/profile.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = "http://localhost:3000/auth/";

  currentUser: BehaviorSubject<Profile|null> = new BehaviorSubject<Profile|null>(null)
  acesstoken:string = ""
  constructor(private http:HttpClient) { }

  signin(user:User){
    return this.http.post(`${this.url}login`,user).pipe(map((data:any) => {


      this.currentUser.next(data.user)
      this.acesstoken = data.accesstoken;
    }))
  }

  signUp(user:Profile){


    return this.http.post(`${this.url}register`,user).pipe(map((data:any) => {
      console.log(data.user);

      this.currentUser.next(data.user)
      this.acesstoken = data.accesstoken;
    }))

  }

  fetchUser(id:number){
    // return this.backend.fetchUser(id)
  }

  getcurrentUser(){
    return this.currentUser
  }

  getaccesstoken(){
    return this.acesstoken;
  }

  logout(){
    this.currentUser.next(null)
    this.acesstoken = ""
  }



  getCurrentUserId(){

  }
}
