import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Route, Router } from '@angular/router';

import { AuthService } from '../auth.service';
import { passwordsvalidation } from '../passwords.directive';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  type = "";

  registerForm = new FormGroup({
    'firstName' : new FormControl('',[Validators.required]),
    'lastName' : new FormControl('',[Validators.required]),
    'email': new FormControl('',[Validators.required]),
    'phoneNumber' :new FormControl('',[Validators.required]),
    'city' :new FormControl('',Validators.required),
    'password' : new FormControl('',[Validators.required,Validators.minLength(8)]),
    'confirmPassword' : new FormControl('',[Validators.required]),
  },{validators:passwordsvalidation})
  constructor(private authService:AuthService,private router:Router) {

  }


  ngOnInit(): void {


  }

  submit(){

    const {confirmPassword,...user} = this.registerForm.value

    this.authService.signUp(user).subscribe(
      value => {
        this.router.navigateByUrl('')

      },
      error => {
        alert("email already exists")
      }
      );

  }


  /******************* getters ******************/
  getfirstName(){
    return this.registerForm.controls['firstName']
  }

  getlastName(){
    return this.registerForm.controls['lastName']
  }

  getemail(){
    return this.registerForm.controls['email']
  }

  getphoneNumber(){
    return this.registerForm.controls['phoneNumber']
  }

  getcity(){
    return this.registerForm.controls['city']
  }

  getpassword(){
    return this.registerForm.controls['password']
  }

  getconfirmPassword(){
    return this.registerForm.controls['confirmPassword']
  }

  selectType(type:string){
    this.type = type;
  }

  getType(){
    return this.type;
  }
}

