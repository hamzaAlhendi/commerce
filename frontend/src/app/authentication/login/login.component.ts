import { Component, OnInit } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoggingService } from 'src/app/logging/logging.service';
import { Profile } from 'src/app/models/profile.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService,private router:Router,private tostar:ToastrService,private loggingService:LoggingService) { }

  ngOnInit(): void {
  }

  onSubmit(form:NgForm){
    this.authService.signin(form.value).subscribe((value:any)=>{
      this.loggingService.success('Logged in successfully',{username:form.value.email})
      this.tostar.success('Logged in successfully',undefined,{timeOut:1000})
      this.router.navigateByUrl('')
    },error =>{
      this.loggingService.error('Invalid username or password')
      console.error(error)
    })
  }
}
