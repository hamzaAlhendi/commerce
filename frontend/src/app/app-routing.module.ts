import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationRoutingModule } from './authentication/authentication-routing.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { HomePageComponent } from './home-page/home-page.component';
import { ProductspageComponent } from './productspage/productspage.component';

const routes: Routes = [
  {
    path:"",
    component:HomePageComponent,
    children:[
      {
        path:"",
        component:ProductspageComponent
      }
    ],
  },

  {
    path:"authentication",
    loadChildren:()=> import('./authentication/authentication.module').then(m=> m.AuthenticationModule)
  },
  {
    path:"profile",
    loadChildren:()=> import('./user/user.module').then(m=> m.UserModule)

  },
  {
    path:"**",
    pathMatch:'full',
    redirectTo:''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
