import { Injectable } from '@angular/core';
import { AuthService } from '../authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

constructor(private authService:AuthService) { }

getcurrentUserName(){
  let name = ""
   this.authService.getcurrentUser().subscribe(value=> {
    value? name = value?.firstName+value?.lastName:""
  })
  return name
}

getcurrentUserEmail(){
  let email = ""
   this.authService.getcurrentUser().subscribe(value=> {
    value? email = value.email:""
  })
  return email
}

getcurrentUserId():string|null{
  let id = null ;
  this.authService.getcurrentUser().subscribe(value=> {
    value? id = value?.id:""
  })

  return id;
}


}
