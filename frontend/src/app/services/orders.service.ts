import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoggingService } from '../logging/logging.service';
import { ProductsService } from './products.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {



  constructor(private http:HttpClient,
     private userService:UserService,
     private productsService:ProductsService,
     private logger:LoggingService,
     private router:Router,
     private toastr:ToastrService
     ) { }

  getUserOrders(){
    let id = this.userService.getcurrentUserId()
    return this.http.get(`http://localhost:3000/order/userorders/${id}`)
  }

  saveOrder(){
    let uId = this.userService.getcurrentUserId();
    let totalPrice = 0;
    const productsToSave = this.productsService.getCurrentUserProducts().map((item) => {
      console.log(item);
      totalPrice += item.counter*item.product.price;
      return {counter:item.counter,productId: item.product.productId}
    })
    let order = {userId:uId,products:productsToSave,price:totalPrice}
    console.log(order);

    this.http.post('http://localhost:3000/order/save',order).subscribe(value=>{
      this.productsService.removeAllProductsFromList();
      this.logger.success('order is submitted successfully',{username:this.userService.getcurrentUserEmail()})
      this.toastr.success('order is submitted successfully','Submitted', {timeOut:2000,positionClass:'toast-bottom-right'})

      this.router.navigateByUrl('')
    }
    ,(error)=> console.error(error))
  }
}


