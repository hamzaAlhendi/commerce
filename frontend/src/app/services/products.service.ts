import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../authentication/auth.service';
import { LoggingService } from '../logging/logging.service';
import { Product } from '../models/product.model';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService implements OnInit {

  url = "http://localhost:3000/product/all"

  products : Product[] = []


  footerContent = [
    {key:"SERVICE",values:["Online Help",
    "Contact Us",
    "Order Status",
    "Change Location",
    "FAQ’s"]},
    {key:"QUOCK SHOP",values:["T-Shirt",
    "Mens",
    "Womens",
    "Gift Cards",
    "Shoes"]},
    {key:"POLICIES",values:["Terms of Use",
      "Privecy Policy",
      "Refund Policy",
      "Billing System",
      "Ticket System"]},
    {key:"ABOUT SHOPPER",values:["Company Information",
    "Careers",
    "Store Location",
    "Affillate Program",
    "Copyright"]}
  ]


  currentUserProducts : {userName:string,products:{counter:number,product:Product}[]} = {userName:"",products:[]}


  constructor(private http:HttpClient,
    private userService:UserService,
    private router:Router,
    private toastr:ToastrService,
    private logger:LoggingService) {
    this.loadProducts()

   }

   ngOnInit(): void {

   }


  getproducts(){

    return this.http.get(this.url);
  }


  getfooterContent(){
    return this.footerContent;
  }

  loadProducts(){

  }

  addToCurrentUserProducts(product:Product){

    if(this.currentUserProducts.userName == ""){
      let name = this.userService.getcurrentUserName()

      this.currentUserProducts.userName = name
    }
    let index = this.currentUserProducts.products.findIndex((storedProduct) => storedProduct.product.productId == product.productId)


    if (index == -1)
      this.currentUserProducts.products.push({counter:1,product})
    else
      this.currentUserProducts.products[index].counter++;
  }

  getCurrentUserProducts(){

    return this.currentUserProducts.products
  }

  removeAllProductsFromList(){
    this.currentUserProducts.products = []
  }

  removeProductFromTheList(product:Product){
    console.log(product);

    let index = this.currentUserProducts.products.findIndex((storedProduct) => storedProduct.product.productId == product.productId)

    this.currentUserProducts.products.splice(index,1)

  }


  getProductsTotalPrice(){
    let price = 0;
    this.currentUserProducts.products.map((product) => {
      price +=  product.counter * product.product.price
    })
    return price
  }





  getProductAttributes(){
    return ['name','photo','price','counter','action']
  }


}
