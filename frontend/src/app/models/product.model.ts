

export class Product {
  productId:number;
  name:string;
  photoUrl:string;
  price:number;
  categori:string;
  brand:string;
  company:string;

  constructor(){
    this.productId = -1
    this.name =""
    this.brand =""
    this.categori =""
    this.company =""
    this.photoUrl =""
    this.price = 0
  }

}
