export interface Profile{
  id:string;
  email:string;
  password:string;
  firstName : string;
  lastName : string;
  photoUrl : string;
  phoneNumber : string;
  city : string;


}
