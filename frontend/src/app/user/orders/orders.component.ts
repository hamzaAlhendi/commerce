import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Product } from 'src/app/models/product.model';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders :any[] = []
  lowValue = 0;
  highValue = 5;

  constructor(private ordersService:OrdersService) {
    this.ordersService.getUserOrders().subscribe((value:any) => {
      console.log("orders")
      console.log(value )
      this.orders = value
    })
  }
  ngOnInit(): void {

  }

  nextPage(event: PageEvent){
    this.lowValue = event.pageIndex * event.pageSize > this.orders.length ? this.lowValue :  event.pageIndex * event.pageSize;
    this.highValue = this.lowValue + event.pageSize > this.orders.length ? this.orders.length : this.lowValue+event.pageSize;
    console.log(this.lowValue,this.highValue)
    return event;
  }


}
