import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { ProfileNavComponent } from './profile-nav/profile-nav.component';
import { MatButtonModule } from '@angular/material/button';
import { OrdersComponent } from './orders/orders.component';
import { CartComponent } from './cart/cart.component';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { GlobalModule } from '../global/global.module';


@NgModule({
  declarations: [
    ProfileComponent,
    ProfileNavComponent,
    OrdersComponent,
    CartComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    GlobalModule
  ]
})
export class UserModule { }
