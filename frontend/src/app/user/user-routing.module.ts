import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserLoggedInGuard } from '../Guards/user-logged-in.guard';
import { CartComponent } from './cart/cart.component';
import { OrdersComponent } from './orders/orders.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path:"",
    component:ProfileComponent,
    canActivate:[UserLoggedInGuard],
    children:[
      {
        path:'orders',
        component:OrdersComponent
      },
      {
        path:'cart',
        component:CartComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
