import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product.model';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductsService } from 'src/app/services/products.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products:{counter:number,product:Product} [] = []
  displayedColumns: string[] = []
  constructor(public productsService:ProductsService,private toastr:ToastrService,private ordersService:OrdersService) {
    this.products = this.productsService.getCurrentUserProducts()

    this.displayedColumns = this.productsService.getProductAttributes();

   }



  dataSource = new MatTableDataSource<{counter:number,product:Product}>(undefined);
  // @ViewChild(MatPaginator) paginator: MatPaginator | null = null;




  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;

  }
  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<{counter:number,product:Product}>(this.products)

  }

  deleteProduct(product:Product){
    this.productsService.removeProductFromTheList(product)
    this.products = this.productsService.getCurrentUserProducts();
    this.dataSource = new MatTableDataSource<{counter:number,product:Product}>(this.products)

  }


  totalPrice(){
    return this.productsService.getProductsTotalPrice()
  }

  saveOrder(){
    this.ordersService.saveOrder()
  }
}
