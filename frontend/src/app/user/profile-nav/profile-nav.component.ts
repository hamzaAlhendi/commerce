import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-nav',
  templateUrl: './profile-nav.component.html',
  styleUrls: ['./profile-nav.component.css']
})
export class ProfileNavComponent implements OnInit {

  constructor() { }

  user = {
    firstName:"hamza",
    lastName:"alhendi",
    photoUrl:"https://cdn.pixabay.com/photo/2012/04/01/18/22/user-23874_960_720.png"
  }
  ngOnInit(): void {
  }

  logout(){
    console.log("logout")
  }

}
