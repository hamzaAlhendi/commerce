import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  footerContent:{key:string,values:string[]}[] = [];

  constructor(private productService:ProductsService) {
    this.footerContent = this.productService.getfooterContent();
   }

  ngOnInit(): void {
  }

}
