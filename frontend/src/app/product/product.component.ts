import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { timeout } from 'rxjs';
import { AuthService } from '../authentication/auth.service';
import { LoggingService } from '../logging/logging.service';
import { Product } from '../models/product.model';
import { ProductsService } from '../services/products.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product :Product = new Product()


  constructor(private productsService:ProductsService,private userService:UserService,private toastr:ToastrService,private loggingService:LoggingService) { }


  ngOnInit(): void {
  }

  addtoCart(){
    if( this.userService.getcurrentUserName() != "")
      this.productsService.addToCurrentUserProducts(this.product)
      
    else{
      this.loggingService.error('you are not logged in',{key:'error',value:'you are not logged in'})
      this.toastr.error('You Are not logged in !','Error',{timeOut:2000,positionClass:'toast-bottom-right'})
    }

  }
}
