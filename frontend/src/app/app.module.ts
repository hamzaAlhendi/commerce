import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { HomePageComponent } from './home-page/home-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductComponent } from './product/product.component';
import { ProductsService } from './services/products.service';
import { SidenavComponent } from './sidenav/sidenav.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductspageComponent } from './productspage/productspage.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import { UserService } from './services/user.service';
import { UserModule } from './user/user.module';
import { OrdersService } from './services/orders.service';
import {MatBadgeModule} from '@angular/material/badge';
import { ToastrModule } from 'ngx-toastr';
import { LoggingService } from './logging/logging.service';
import { LogPublishersService } from './logging/log-publishers.service';
import { GlobalModule } from './global/global.module';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ProductComponent,
    NavbarComponent,
    SidenavComponent,
    ProductspageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthenticationModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatTableModule,
    UserModule,
    MatBadgeModule,
    ToastrModule.forRoot(),
    GlobalModule
  ],
  providers: [ProductsService,UserService,OrdersService,LoggingService,LogPublishersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
