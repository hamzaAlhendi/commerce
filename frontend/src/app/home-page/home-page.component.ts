import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authentication/auth.service';
import { Product } from '../models/product.model';
import { Profile } from '../models/profile.model';
import { User } from '../models/user.model';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {


  footerContent:{key:string,values:string[]}[] = [];


  constructor(private httpClient:HttpClient,public authService:AuthService, private productService:ProductsService) {

    this.footerContent = this.productService.getfooterContent();

  }

  ngOnInit(): void {


  }

}
